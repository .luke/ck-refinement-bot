## Create:Knowledge backlog refinement

#### Set up

Clone this repository, and install the gems:

```
bundle
```

#### API token

Generate a GitLab API token, enabling:

- `api`
- `read_api`

#### Running

You can test what will happen with the `--dry-run` option:

```
bundle exec gitlab-triage --dry-run --token <your-api-token> --source-id gitlab-org/gitlab
```

Then to create the issue run the above without `--dry-run`.

#### Editing

See the `.triage-policies.yml` file and [`gitlab-triage` docs](https://gitlab.com/gitlab-org/gitlab-triage).
